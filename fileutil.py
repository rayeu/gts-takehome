
import struct


"""
StringUnpacker is a class that provides a file like interface to provided string buffer.
The offset is maintained by the class itself, hence the reader can proceed as if he/she
was reading a file. The user is thrown an exception if the current string buffer's size
is less than the header length read. The user is responsible for appending new string to
the string buffer before proceeding to parse the data again.
"""
class StringUnpacker(object):
    def __init__(self):
        self.s = ""
        self.offset = 0

    def append_string(self, new_s):
        self.s += new_s

    def parse_header(self):
        l, n = struct.unpack_from('>hh', self.s[self.offset:self.offset + 4])
        if len(self.s) - self.offset < l:
            return l, n, False
        self.offset += 4
        return l, n, True

    def parse_update_len_type_and_symbol(self):
        res = struct.unpack_from('>hc5s', self.s[self.offset:self.offset + 8])
        self.offset += 8
        return res

    def parse_trade_size_and_price(self):
        res = struct.unpack_from('>hq', self.s[self.offset:self.offset + 10])
        self.offset += 10
        return res

    def skip_bytes(self, n):
        self.offset += n


"""
FileReader is a simple wrapper that provides a generator which generates
chunks of specified size to the reader.
"""
class FileReader(object):
    def __init__(self, filename):
        self.f = open(filename)

    def generate_chunks(self, chunk_size):
        chunk = self.f.read(chunk_size)
        yield chunk


"""
FileWriter is a simple interface which allows writing to file in specified format.
"""
class FileWriter(object):
    def __init__(self, filename, format):
        self.f = open(filename, "w")
        self.format = format

    def write(self, size, symbol, price):
        self.f.write(self.format % (size * 100, symbol.strip(), price / 10000.0))
