
import fileutil
import logging

CHUNK_SIZE = 1024

logging.basicConfig(level=logging.DEBUG)

def parse_file(input_filename, output_filename):
    fr = fileutil.FileReader(input_filename)
    sp = fileutil.StringUnpacker()
    wf = fileutil.FileWriter(output_filename, "%s %s @ %.2f\n")

    quote_data_skipped = 0
    while True:
        s = fr.generate_chunks(CHUNK_SIZE).next()
        sp.append_string(s)
        if not s:
            logging.debug("parse_file; EOF reached.")
            break

        l, n, success = sp.parse_header()
        if not success:
            logging.debug("parse_file; StringUnpacker's buffer is short, need to load more data.")
            continue

        # For each market update, do the following.
        for i in range(n):
            # Parse header and symbol.
            nl, t, sym = sp.parse_update_len_type_and_symbol()
            # If it is a quote data, skip!
            if t == 'Q':
                sp.skip_bytes(nl - 8)
                quote_data_skipped += 1
                continue
            # If it is a trade data, parse size and price, and write output to file.
            if t == 'T':
                ts, tp = sp.parse_trade_size_and_price()
                wf.write(ts, sym, tp)
                sp.skip_bytes(nl - 18)
                logging.debug("parse_file; parsed trade data; size=%s; price=%s", ts, tp)

    logging.debug("parse_file; number of quote data skipped=%s", quote_data_skipped)


def main():
    parse_file("./input.dat", "./output.txt")


main()
