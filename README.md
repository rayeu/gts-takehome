** Steps to run the program **

* Unzip the given archive.
* python main.py


** Files provided **

* main.py - Parses the given input and generates output as requested.
* fileutil.py - Contains libraries created to assist main.py.
* input.dat - Given input.
* output.txt - Generated output.